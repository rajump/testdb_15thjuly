﻿-- <Migration ID="e0de8180-4fee-40c2-8a89-5f07b24f4ed0" />
GO

PRINT N'Creating [dbo].[emp-table]'
GO
CREATE TABLE [dbo].[emp-table]
(
[emp-name] [nchar] (10) NULL,
[emp-no] [nchar] (10) NULL,
[emp-dept] [nchar] (10) NULL
)
GO
